<?php
/*
by donshakespeare
TinymceWrapperMarkdown is an Output Modifier for MODX
[[*content:TinymceWrapperMarkdown=`markdown`]]
or `markdownE` or `parsedown`
default =`parsedownE`
parsedownE or markdownE is good for Markdown mixed with HTML, and Markdown enclosed in HTML markdown="1"*/
if($input){
    $content = $input;
    // Parse MODX tags first || parse BBCodes here too
    $content = $modx->newObject('modChunk')->process(null, $content);
    if($options == "markdown"){
        if (!class_exists('\Michelf\Markdown')) {
            require MODX_ASSETS_PATH . 'components/tinymcewrapper/markdown/Michelf/Markdown.inc.php';
        }
        $content = \Michelf\Markdown::defaultTransform($content);
    }
    elseif($options == "markdownE"){
        if (!class_exists('\Michelf\MarkdownExtra')) {
            require MODX_ASSETS_PATH . 'components/tinymcewrapper/markdown/Michelf/MarkdownExtra.inc.php';
        }
        $content = \Michelf\MarkdownExtra::defaultTransform($content);
    }
    elseif($options == "parsedown"){
        require MODX_ASSETS_PATH . 'components/tinymcewrapper/markdown/parsedown/Parsedown.php';
        $Parsedown = new Parsedown();
        $content = $Parsedown->text($content);
    }
    else{ //default state
        $options = "parsedownExtra";
        require MODX_ASSETS_PATH . 'components/tinymcewrapper/markdown/parsedown/Parsedown.php';
        require MODX_ASSETS_PATH . 'components/tinymcewrapper/markdown/parsedown/ParsedownExtra.php';
        $ParsedownExtra = new ParsedownExtra();
        $content = $ParsedownExtra->text($content);
    }
    return $content;
}