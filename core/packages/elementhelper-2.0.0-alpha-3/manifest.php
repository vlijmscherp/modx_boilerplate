<?php return array (
  'manifest-version' => '1.1',
  'manifest-attributes' => 
  array (
    'license' => 'Element Helper - A MODx plugin for automatically creating elements
from static files without the manager.

Copyright (C) 2014  Rory Gibson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

http://www.gnu.org/licenses/',
    'readme' => '=====================
Extra: Element Helper
=====================

Version: 2.0.0
 
Element Helper is a MODx Revolution plugin for automatically creating elements from static files without the MODx manager.

For instructions on using ElementHelper please see - https://github.com/roryg/ElementHelper',
    'changelog' => '====================
Version 2.0.0
====================

- Initial release',
  ),
  'manifest-vehicles' => 
  array (
    0 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modNamespace',
      'guid' => 'b88c3e5a53e10f446bcc3f57aaeed391',
      'native_key' => 'elementhelper',
      'filename' => 'modNamespace/c6389d226d71d6db6102d0f5355c86e9.vehicle',
      'namespace' => 'elementhelper',
    ),
    1 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'c0cd6fda28d0bb7852c32da2b7acf10e',
      'native_key' => 'elementhelper.chunk_path',
      'filename' => 'modSystemSetting/a11c5c50e6b06d5b1ef8d27bfaaf3919.vehicle',
      'namespace' => 'elementhelper',
    ),
    2 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '6faeb54f1820cc723ca821e0e972fc85',
      'native_key' => 'elementhelper.template_path',
      'filename' => 'modSystemSetting/2cb2d1372d073defe6bec6ac0466f7d4.vehicle',
      'namespace' => 'elementhelper',
    ),
    3 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '31d886864f5b2427920a1b5fafbda90c',
      'native_key' => 'elementhelper.plugin_path',
      'filename' => 'modSystemSetting/9e916c87c9e47557f80ee2a6321603e0.vehicle',
      'namespace' => 'elementhelper',
    ),
    4 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '9bc443cec5daf4e433b570d0a4b2dd7f',
      'native_key' => 'elementhelper.snippet_path',
      'filename' => 'modSystemSetting/86ebe693520be432df50818c3bc98ded.vehicle',
      'namespace' => 'elementhelper',
    ),
    5 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'fef81aa68d97515129fef618bfb9f039',
      'native_key' => 'elementhelper.tv_file_path',
      'filename' => 'modSystemSetting/15b3af2b275ae113daea42333876defe.vehicle',
      'namespace' => 'elementhelper',
    ),
    6 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '18207d051e54a234d31c806ed8cad25c',
      'native_key' => 'elementhelper.element_sync_file_path',
      'filename' => 'modSystemSetting/4d6c22223828c3caf059eb3d223d6586.vehicle',
      'namespace' => 'elementhelper',
    ),
    7 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '728366c1bd88655f4e8e22c1d5c59e7b',
      'native_key' => 'elementhelper.usergroups',
      'filename' => 'modSystemSetting/74da1ab9d06f5de3de7a0c3024b933b7.vehicle',
      'namespace' => 'elementhelper',
    ),
    8 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'c36032d7193a58711b6b21262d2319d3',
      'native_key' => 'elementhelper.tv_access_control',
      'filename' => 'modSystemSetting/422c7fbf4bf9e0fe18525aeee8bc172b.vehicle',
      'namespace' => 'elementhelper',
    ),
    9 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '61c48fad0d6bccec01f1539d31f0bb15',
      'native_key' => 'elementhelper.category_whitelist',
      'filename' => 'modSystemSetting/6d4cbd8b46cc4ae5e58bc982a2afc2cd.vehicle',
      'namespace' => 'elementhelper',
    ),
    10 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '3e8f5c177f57f2311a155c4f2a430011',
      'native_key' => 'elementhelper.element_blacklist',
      'filename' => 'modSystemSetting/5f3456e0e51802a481da5695a18efe58.vehicle',
      'namespace' => 'elementhelper',
    ),
    11 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modCategory',
      'guid' => '3b22f6148b4d33feea7f56be18616ebf',
      'native_key' => 1,
      'filename' => 'modCategory/4dd4bdaecb92a1e515e26c24ad2ea778.vehicle',
      'namespace' => 'elementhelper',
    ),
  ),
);